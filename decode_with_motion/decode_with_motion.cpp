/**
 * This example uses ffmpeg to parse the container format and to decode the resulting bitstream. 
 * I-Frames are fully decoded to RGB frames, P-frames are partially decoded by applying motion vectors to 
 * the previously decoded frame. All frames are saved as PPM images.
 * This code assumes that the underlying codec:
 *  (a) Uses full-frames, i.e., it does not use slices
 *  (b) The FFMPEG implementation supports exporting motion vectors
 *  (c) The codec only references the previous frame (no multiple anchors, no B frames)
 */

// FFMPEG headers are pure C, they need to be surrounded by extern "C" to avoid name mangling
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libavutil/motion_vector.h>
#include <libswscale/swscale.h>
}

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>

// We go back to writing RGB PPMs now
void write_ppm(const std::string &fname, AVFrame *frame) {
    // PPM file format:
    // Line 1: format identifier P6
    // Line 2: width <space> height <space> max_value (255)
    // Line 3-end: raw RGB image data
    std::ofstream output_image;
    output_image.open(fname, std::ios::binary | std::ios::out);
    output_image << "P6\n"
                 << frame->width << " " << frame->height << "\n255\n";

    for (int y = 0; y < frame->height; y++) {
        output_image.write((const char *)frame->data[0] + (y * frame->linesize[0]), frame->width * 3);
    }
}

int main(int argc, char **argv) {
    // Read the container and set up the streams and codecs, see full_decode for more details
    AVFormatContext *format_context = nullptr;

    if (avformat_open_input(&format_context, argv[1], nullptr, nullptr) != 0) {
        std::cout << "Unable to open " << argv[1] << std::endl;
        return -1;
    }

    if (avformat_find_stream_info(format_context, nullptr) < 0) {
        std::cout << "Unable to parse video streams, video file is invalid" << std::endl;
    }

    av_dump_format(format_context, 0, argv[1], 0);

    AVCodec *codec = nullptr;
    int stream = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);

    if (stream < 0) {
        std::cout << "Unable to find a video stream in the file" << std::endl;
        return -1;
    }

    AVCodecParameters *codec_params = format_context->streams[stream]->codecpar;

    AVCodecContext *codec_context = avcodec_alloc_context3(codec);
    avcodec_parameters_to_context(codec_context, codec_params);

    AVDictionary *opts = nullptr;
    av_dict_set(&opts, "flags2", "+export_mvs", 0);

    if (avcodec_open2(codec_context, codec, &opts) < 0) {
        std::cout << "Unable to load codec" << std::endl;
        return -1;
    }

    // Back to needing two frame objects to write normal RGB frames
    AVFrame *nativeFrame = av_frame_alloc(); // Frame as decoded from the video (likely YCbCr)
    AVFrame *rgbFrame = av_frame_alloc();    // RGB Frame

    av_image_alloc(rgbFrame->data, rgbFrame->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
    rgbFrame->width = codec_params->width;
    rgbFrame->height = codec_params->height;

    SwsContext *sws_context = sws_getContext(codec_params->width, codec_params->height, (AVPixelFormat)codec_params->format, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);

    // Parse the stream
    int framenum = 0;
    for (AVPacket packet; av_read_frame(format_context, &packet) >= 0; framenum++) {
        if (packet.stream_index == stream) {
            // Unfortunately the frame still needs to be fully decoded to get motion vectors
            avcodec_send_packet(codec_context, &packet);
            avcodec_receive_frame(codec_context, nativeFrame);

            if (nativeFrame->pict_type == AV_PICTURE_TYPE_I) {
                // I-frame
                // All we need to do for this is convert it to RGB
                sws_scale(sws_context, nativeFrame->data, nativeFrame->linesize, 0, nativeFrame->height, rgbFrame->data, rgbFrame->linesize);
            } else {
                // P-frame
                // Get the motion vectors
                AVFrameSideData *sd = av_frame_get_side_data(nativeFrame, AV_FRAME_DATA_MOTION_VECTORS);
                AVMotionVector *mvs = reinterpret_cast<AVMotionVector *>(sd->data);
                int n = sd->size / sizeof(AVMotionVector);

                // Allocate a temporary frame which is a copy of the previous frame, we're going to
                // use the motion vectors to change blocks in this copy
                AVFrame *tmp = av_frame_alloc();
                av_frame_copy_props(tmp, rgbFrame);
                av_image_alloc(tmp->data, tmp->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
                tmp->width = codec_params->width;
                tmp->height = codec_params->height;

                // Copy the previous frame into the temporary frame
                // Note that this is RGB24, an interlaced format so there aren't separate channels for data and linesize, it's all in one place:
                // RGBRGBRGB (each pixel is 8 bits, 8*3=24bits per pixel)
                for (int y = 0; y < nativeFrame->height; y++) {
                    std::copy_n(rgbFrame->data[0] + (y * rgbFrame->linesize[0]), rgbFrame->width * 3, tmp->data[0] + (y * tmp->linesize[0]));
                }

                // Iterate over the motion vectors
                for (auto it = mvs; it != mvs + n; it++) {
                    // x and y are the offsets on each axis, so we go from 0 to h or w which is the size
                    // of the block that the motion vector refers to
                    // Note that src and dst refer to the CENTER of the block so we need to iteratate from -size/2 to size/2
                    for (int y = -it->h / 2; y < it->h / 2; y++) {
                        for (int x = -it->w / 2; x < it->w / 2; x++) {
                            // compute the source pixel
                            int src_x = it->src_x + x;
                            int src_y = it->src_y + y;

                            if (src_x < 0 || src_x >= nativeFrame->width || src_y < 0 || src_y >= nativeFrame->height) { // Make sure it isnt out of bounds, this CAN happen
                                continue;
                            }

                            // compute the destination pixel
                            int dst_x = it->dst_x + x;
                            int dst_y = it->dst_y + y;

                            if (dst_x < 0 || dst_x >= nativeFrame->width || dst_y < 0 || dst_y >= nativeFrame->height) { // Make sure it also isnt out of bounds
                                continue;
                            }

                            // Convert the pixel locations to offsets into the flat pixel array
                            int src_offset = src_y * rgbFrame->linesize[0] + src_x * 3;
                            int dst_offset = dst_y * tmp->linesize[0] + dst_x * 3;

                            // copy the source pixel in the previous frame to the destination pixel in the current frame (3 bytes)
                            std::copy_n(rgbFrame->data[0] + src_offset, 3, tmp->data[0] + dst_offset);
                        }
                    }
                }

                // Swap the reference frame
                std::swap(rgbFrame, tmp);
                av_frame_unref(tmp);
            }

            // Write the frame to disk
            std::ostringstream fname_format;
            fname_format << "frame" << framenum << ".ppm";

            write_ppm(fname_format.str(), rgbFrame);
        }

        av_packet_unref(&packet);
    }

    av_frame_free(&nativeFrame);

    avcodec_close(codec_context);
    avcodec_free_context(&codec_context);
    avformat_close_input(&format_context);
    avformat_free_context(format_context);

    return 0;
}