from typing import Any, Optional, Sequence
import av
import argparse
from av import Packet
from bitstring import BitStream, ConstBitStream
from enum import Enum
from av.container import InputContainer


class SEIParser:
    def __init__(self, rbsp_bits: BitStream) -> None:
        pass


class NALUnitType(Enum):
    Invalid = -1
    Unspecified = 0
    NonIDRSlice = 1
    DataPartitionA = 2
    DataPartitionB = 3
    DataPartictionC = 4
    IDR = 5
    SupplementalEnhancementInformation = 6
    SequenceParameterSet = 7
    PictureParameterSet = 8
    AccessUnitDelimiter = 9
    EndOfSequence = 10
    EndOfStream = 11
    Filler = 12


rbsp_parsers = {NALUnitType.SupplementalEnhancementInformation: SEIParser}


class AVCCSequenceHeader:
    avc_profile: int = 0
    avc_compatibility: int = 0
    avc_level: int = 0
    nalu_size_bytes: int = 0

    num_sps: int = 0
    sps_data: Sequence[Any] = None  # TODO

    num_pps: int = 0
    pps_data: Sequence[Any] = None  # TODO

    def __init__(self, sh: bytes):
        sh_bits = ConstBitStream(sh)

        # Version, 1 byte, always 0x01, discard it
        sh_bits.read(8)

        self.avc_profile = sh_bits.read("uint:8")
        self.avc_compatibility = sh_bits.read("uint:8")
        self.avc_level = sh_bits.read("uint:8")

        # 6 reserved bits
        sh_bits.read(6)

        # This is simplified from the version which is stored in the header
        self.nalu_size_bytes = sh_bits.read("uint:2") + 1

        # 3 reserved bits
        sh_bits.read(3)

        # TODO parse sps and pps data from this point


class H264NALUnit:
    """
    Parses an H264 (MPEG-4 part 10/AAC) NALU in Annex B or AVCC format.
    """

    nal_ref_idc: int = 0
    nal_unit_type: NALUnitType = NALUnitType.Invalid
    total_bytes: int = 0
    implementation: Any = None

    def __init__(self, p: Packet, format: str = "annexb", sequence_header: Optional[AVCCSequenceHeader] = None) -> None:
        nal_bits = ConstBitStream(p.to_bytes())  # TODO can we avoid the copy here?

        self.total_bytes = len(nal_bits) // 8

        if self.total_bytes == 0:
            return

        if format == "annexb":
            # Look for the start code
            while nal_bits.peek(24) != "0x000001" and nal_bits.peek(32) != "0x00000001":
                nal_bits.read(8)

            # Start code was in the next 32 or 24 bytes, if it was 32 we discard an additional byte
            if nal_bits.peek(24) != "0x000001":
                nal_bits.read(8)

            # Discard the start code
            nal_bits.read(24)
        elif format == "avcc":
            # AVCC packets start with the size of the packet, we can discard it as we don't need it
            if sequence_header is not None:
                nal_bits.read(sequence_header.nalu_size_bytes * 8)
            else:
                nal_bits.read(32)  # Usually can safely assume 4 bytes so use that as a fallback

        # Start parsing the NAL unit itself
        nal_bits.read(1)  # Forbidden zero bit, assume for now that it's OK
        self.nal_ref_idc = nal_bits.read("uint:2")
        self.nal_unit_type = NALUnitType(nal_bits.read("uint:5"))

        # Next create the Raw Byte Sequence Payload (RBSP) by removing emulation prevention bytes
        # TODO too slow
        # rbsp_bits = BitStream()
        # while nal_bits.pos < len(nal_bits):
        #     if nal_bits.pos + 24 < len(nal_bits) and nal_bits.peek(24) == "0x000003":
        #         rbsp_bits.append(nal_bits.read("bytes:2"))
        #         nal_bits.read("bytes:1")  # discard the emulation prevention byte
        #     else:
        #         rbsp_bits.append(nal_bits.read("bytes:1"))

        # TODO Parse the RBSP, this is dependant on the NAL unit typek


def identify_stream_container(container: InputContainer, stream: int = 0) -> str:
    stream = container.streams.video[args.stream]
    ed_bits = ConstBitStream(stream.codec_context.extradata)

    return "avcc" if ed_bits.read(8) == "0x01" else "annexb"  # Supposedly this is enough to identify the AVCC sequence header


parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("--stream", type=int, default=0, required=False)
args = parser.parse_args()

container = av.open(args.input)
stream_container = identify_stream_container(container, args.stream)

if stream_container == "avcc":
    sequence_header = AVCCSequenceHeader(container.streams.video[args.stream].codec_context.extradata)
    print(f"Profile {sequence_header.avc_profile}")
    print(f"Level {sequence_header.avc_level}")
else:
    sequence_header = None

for packet in container.demux(video=args.stream):
    if packet.size > 0:
        nalu = H264NALUnit(packet, format=stream_container, sequence_header=sequence_header)
        print(f"===NALU: Length {nalu.total_bytes} bytes, PTS {packet.pts}===")
        print(f"NAL Unit Type: {nalu.nal_unit_type}")
