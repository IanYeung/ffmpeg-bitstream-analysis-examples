import argparse
import timeit

import av
import torch
import torchvision.io
from av.sidedata.sidedata import SideData
from av.video.frame import PictureType
from numpy.lib.recfunctions import structured_to_unstructured
from torch import Tensor
from torchvision.transforms.functional import to_tensor
from torchvision.utils import draw_bounding_boxes


def extract_mvs(sd: SideData) -> Tensor:
    mv_data = structured_to_unstructured(sd.to_ndarray(), dtype=float)
    mv_data = torch.tensor(mv_data[:, 1:])
    return mv_data


def vis_motion(cur_frame: Tensor, prev_frame: Tensor, motion: Tensor) -> Tensor:
    src_boxes = []
    dst_boxes = []
    labels = []
    for i in range(len(motion)):
        mv = motion[i]
        mb_size = mv[0:2]

        src = mv[2:4]
        dst = mv[4:6]

        src_tl = src - mb_size // 2
        dst_tl = dst - mb_size // 2

        src_br = src + mb_size // 2
        dst_br = dst + mb_size // 2

        src_boxes.append(torch.cat([src_tl, src_br]))
        dst_boxes.append(torch.cat([dst_tl, dst_br]))

        motion_params = mv[7:9]
        ms = mv[9]

        motion_amount = torch.round(torch.linalg.norm(motion_params / ms)).int()
        labels.append(f"{motion_amount.item()}")

    src_boxes = torch.stack(src_boxes)
    dst_boxes = torch.stack(dst_boxes)

    return draw_bounding_boxes(prev_frame.byte(), src_boxes, labels=labels), draw_bounding_boxes(cur_frame.byte(), dst_boxes, labels=labels, font_size=5)


parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("--stream", type=int, default=0, required=False)
args = parser.parse_args()

container = av.open(args.input)

stream = container.streams.video[args.stream]
stream.codec_context.flags2 |= av.codec.context.Flags2.EXPORT_MVS

frame_iterator = container.decode(video=args.stream)

# extract the first frame, it should be an I frame
iframe_vid = next(frame_iterator)
assert iframe_vid.pict_type == PictureType.I

prev_frame_pix = torch.tensor(iframe_vid.to_rgb().to_ndarray()).byte().permute(2, 0, 1)

frame = next(frame_iterator)

cur_frame_pix = torch.tensor(frame.to_rgb().to_ndarray()).byte().permute(2, 0, 1)


mvs = extract_mvs(frame.side_data[0])

src_frame, dst_frame = vis_motion(cur_frame_pix, prev_frame_pix, mvs)


torchvision.io.write_png(src_frame, f"source.png")
torchvision.io.write_png(dst_frame, f"dest.png")
