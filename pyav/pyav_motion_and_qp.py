import argparse
from dataclasses import dataclass
from importlib.metadata import metadata
from math import ceil

import av
import numpy as np
import torch
from av import VideoFrame
from numpy.lib.recfunctions import structured_to_unstructured

parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("--stream", type=int, default=0, required=False)
args = parser.parse_args()

container = av.open(args.input)

stream = container.streams.video[0]
stream.codec_context.options["export_side_data"] = "venc_params"
stream.codec_context.flags2 |= av.codec.context.Flags2.EXPORT_MVS

print(stream.coded_height, stream.coded_width)

iterator = container.decode(video=args.stream)
print(next(iterator))


@dataclass
class VideoEncParams:
    metadata: np.ndarray
    delta_qp: np.ndarray
    blocks: np.ndarray

    def __init__(self, data: av.sidedata.sidedata.SideData) -> None:
        self.metadata = np.frombuffer(
            data, count=1, dtype=np.dtype([("nb_blocks", "uint32"), ("blocks_offset", "uint64"), ("blocks_size", "uint64"), ("qp_type", "uint32"), ("qp", "int32")], align=True)
        )

        self.delta_qp = np.frombuffer(data, offset=32, count=8, dtype="int32").reshape(1, 4, 2)
        self.blocks = np.frombuffer(data, offset=self.metadata["blocks_offset"].item(), dtype=np.dtype([("src_x", "int32"), ("src_y", "int32"), ("w", "int32"), ("h", "int32"), ("delta_qp", "int32")]))

    def to_ndarray(self) -> np.ndarray:
        channel_qps = np.expand_dims(self.blocks["delta_qp"], axis=(1, 2)) + self.delta_qp + self.metadata["qp"]
        return channel_qps


for frame in iterator:
    print(frame.pict_type)
    for sd in frame.side_data:
        if int(sd.type) == 21:
            mb_width, mb_height = ceil(frame.width / 16), ceil(frame.height / 16)

            vencparams = VideoEncParams(sd)
            qpa = vencparams.to_ndarray()
            qpt = torch.tensor(qpa, names=["blocks", "channels", "qp"])
            qpt = qpt.unflatten("blocks", (("mb_height", mb_height), ("mb_width", mb_width)))  # this assumes row major order
            qpt = qpt.align_to("channels", "mb_height", "mb_width", "qp")[..., 0]  # discard AC coefficient QP

            print(f"Block 0 has qps: {qpt.shape}")
            exit()
        elif sd.type == av.sidedata.sidedata.Type.MOTION_VECTORS:
            nda = structured_to_unstructured(sd.to_ndarray(), dtype=float)

            ta = torch.tensor(nda[:, 1:7], names=["n_vectors", "fields"])
