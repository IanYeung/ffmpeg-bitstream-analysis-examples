"""
This code shows how to vectorize motion compensation and compares it to the loop method

Computing motion compensation requires reading off the motion vectors from the curent frame 
and "applying" them to the previous frame. 

To review: motion compensation divides the *current* frame into a nonoverlapping grid of macroblocks. For h264, 
the macroblocks can be 16x16, 8x8, 16x8, or 8x16. While the grid is non-overlapping it may not cover the frame 
(i.e, there may be holes). Each macroblock has a motion vector which denotes where in the *previous* frame that 
block came from. In the previous frame, the blocks can, and often do, overlap.

The motion vectors hold roughly the following information:

source x, y - the *center* of the block in the previous frame
destination x, y - the *center* of the block in the current frame
block width, height - the width and height of the block

There are often many of these in a single frame. 

We compute the motion frame by cloning the previous frame and then for each block, copying the pixels that it
covers in the source frame into the motion frame at the destination location. 

We're working in pytorch, this can be done naively using a for loop, simply looping over each motion vector and
copying the data (pseudocode):

motion_frame = prev_frame
for mv in motion_vectors:
    motion_frame[:, mv.dy - mv.h/2:mv.dy + mv.h/2, mv.dx - mv.w/2: mv.dx + mv.w/2] = prev_frame[:, mv.sy - mv.h/2:mv.sy + mv.h/2, mv.sx - mv.w/2: mv.sx + mv.w/2]

but this is quite slow because of the loop. Instead we can vectorize the operations, this is much faster 
although the procedure is quite complex. In testing, the loop method computed the motion frame at about 10fps vs
the vetorized methods about 90fps.

The high level idea is to compute the indices belonging to each block. In other words, if a block has source center (100, 100) and
size (8, 8), then all indices in 96-104 width and 96-104 height are stored in a tensor for that block (along wih the corresponding
destination indices). Then with these indices, the function `gather` can be used to extract the pixels at the indices and the function
`scatter` can be used to write them back. The obvious problem here is that the block sizes are not all the same, so there's a variable 
number of indices per block which means they can't be stored in a tensor. To get around that, we can divide the motion vectors by block 
size and work with these four separate tensors. At the end of the procedure, they are all linearized into a vector of indices (blocks no 
longer matter at this point, we only are about which pixels to copy) and they can all be merged into one vector. 

The procedure starts by splitting the mvs up by block size. Next, we can compute
offsets for each block from the center point (note that these are constants per block size, e.g. for 8x8 we have -4, -3, -2, ... 3 for both width 
and height). To apply these offsets, we tile the center coord so that it makes the full block (e.g., if the center coord is 8,10 we have
[8,10],[8,10] ... [8,10]) and add the offsets. This gives is the pixel indices for each pixel the block. Next we can simply linearize these 
(y * image_width + x), flatten, and concat the different block sizes together. The source indices are then input to gather to extract the 
pixels and the destination indices are used with scatter to write them.

See `motion_compensation_vectorized` for this procedure and `motion_compensation_loop` for the naive procedure.

"""

import argparse
import timeit
from typing import Sequence

import av
import torch
from av.sidedata.sidedata import SideData
from av.video.frame import PictureType
from numpy.lib.recfunctions import structured_to_unstructured
from torch import Tensor
from torch.nn.functional import pad
from torchvision.io import write_video
from torchvision.transforms.functional import to_tensor


def extract_mvs(sd: SideData) -> Tensor:
    mv_data = structured_to_unstructured(sd.to_ndarray(), dtype=float)
    mv_data = torch.as_tensor(mv_data[:, 1:7], dtype=int)
    return mv_data


def motion_compensation_vectorized(source_volume: Tensor, mvs: Tensor, frame_size: Tensor) -> Tensor:
    device = source_volume.device

    n_volumes = mvs.shape[0]
    n_frames = mvs.shape[1]
    n_vectors = mvs.shape[2]
    n_channels = frame_size[2]

    # extract some info from the mv vector
    block_sizes = mvs[..., 0:2]

    mvs_src = mvs[..., 2:4]
    mvs_dst = mvs[..., 4:6]

    # convert src and dst to quadruple indicating: batch position, frame, x, y
    batch_indices = torch.arange(n_volumes, device=device)
    frame_indices = torch.arange(n_frames, device=device)

    batch_pos = batch_indices.view(-1, 1, 1).expand(-1, n_frames, n_vectors).view(n_volumes, n_frames, n_vectors, 1)
    frame_pos = frame_indices.view(1, -1, 1).expand(n_volumes, -1, n_vectors).view(n_volumes, n_frames, n_vectors, 1)

    bp_fp = torch.cat([batch_pos, frame_pos], dim=-1)

    # mvs_src = torch.cat([batch_pos, frame_pos, mvs_src], dim=-1)
    # mvs_dst = torch.cat([batch_pos, frame_pos, mvs_dst], dim=-1)

    # Split into different block sizes
    # options are 8x8, 16x16, 16x8, 8x16
    block_options = [torch.as_tensor([8, 8], device=device), torch.as_tensor([16, 16], device=device), torch.as_tensor([16, 8], device=device), torch.as_tensor([8, 16], device=device)]
    selected_blocks = [(block_sizes == b).all(-1).unsqueeze(-1) for b in block_options]

    # Make offsets for each of the different block sizes (offsets from center pixel)
    idxs_8 = torch.arange(-4, 4, device=device)
    idxs_16 = torch.arange(-8, 8, device=device)
    offsets = [
        torch.stack(torch.meshgrid(idxs_8, idxs_8)),
        torch.stack(torch.meshgrid(idxs_16, idxs_16)),
        torch.stack(torch.meshgrid(idxs_16, idxs_8)),
        torch.stack(torch.meshgrid(idxs_8, idxs_16)),
    ]

    # flatten a 4D index
    def index_4d(b, f, x, y, nf, w, h):
        return x + w * (y + h * (f + b * nf))

    # This function compute the linearized positions for *each* individual pixel that moved (was in a motion vector's macroblock)
    # it simply converts the center points of each block to a list of indices
    # do this for the source and destination and the indices correspond, for a pixel in the source frame you have the pixel that it was moved to in the destination frame (flattened)
    def compute_linearlized_blocks(mvs_loc: Tensor) -> Tensor:
        # select TL coords of motion vectors for the block sizes and tile the TL coords to match the full size of the block (e.g., the same TL coord repeated 16x16 times) TODO
        mvs_selected = [mvs_loc.masked_select(b_sel).view(-1, 1, 1).tile(1, *b_sz).view(-1, 2, *b_sz) for b_sel, b_sz in zip(selected_blocks, block_options)]

        # add in the offsets for each coord (I permute here to make bounds checking work, basically just moving the 2D index to the end) TODO
        mvs_blocks = [(mv_b + offset_b).permute(0, 2, 3, 1) for mv_b, offset_b in zip(mvs_selected, offsets)]

        # check bounds
        mvs_blocks = [mv_b.where(mv_b > 0, torch.as_tensor(0, device=device)) for mv_b in mvs_blocks]
        mvs_blocks = [mv_b.where(mv_b < frame_size[-2:].flip(dims=[0]), frame_size[-2:].flip(dims=[0]) - 1) for mv_b in mvs_blocks]

        # select the batch and frame positions according to the same blocks and tile them to match the motion vectors
        bp_fp_selected = [bp_fp.masked_select(b_sel).view(-1, 1, 1).tile(1, *b_sz).view(-1, 2, *b_sz).permute(0, 2, 3, 1) for b_sel, b_sz in zip(selected_blocks, block_options)]

        # linearize the indices, flatten the container, and concat all the block sizes together. This is a 4D index of batch, frame, x, y.
        mvs_linear = (
            torch.cat([index_4d(bp_fp_b[..., 0], bp_fp_b[..., 1], mv_b[..., 0], mv_b[..., 1], n_frames, frame_size[-1], frame_size[-2]).flatten() for bp_fp_b, mv_b in zip(bp_fp_selected, mvs_blocks)])
            .view(1, -1)
            .expand(n_channels, -1)
        )
        return mvs_linear

    # compute the source and destination pixel indices
    mvs_src_linear = compute_linearlized_blocks(mvs_src)
    mvs_dst_linear = compute_linearlized_blocks(mvs_dst)

    # Flatten the source volume and extract all the pixels that were indexed. This operation is the same for each channel, so we move the channel index to the front and ignore it
    source_frame_blocks = source_volume.permute(2, 0, 1, 3, 4).reshape(n_channels, -1).gather(1, mvs_src_linear)

    # Make the motion frame and flatten it
    motion_frame = torch.clone(source_volume).permute(2, 0, 1, 3, 4).reshape(n_channels, -1)

    # Copy the pixels extracted from the source frame to their positions in the motion frame
    motion_frame.scatter_(1, mvs_dst_linear, source_frame_blocks)

    # Reshape the motion frame to be an image
    motion_frame = motion_frame.view(n_channels, n_volumes, n_frames, *frame_size[-2:]).permute(1, 2, 0, 3, 4)

    return motion_frame


def motion_compensation_loop(prev_frame: Tensor, motion: Tensor) -> Tensor:
    frame_size = torch.tensor(prev_frame.shape[3:], device=device).flip(0)

    batches = prev_frame.shape[0]
    n_frames = prev_frame.shape[1]

    # Compute the motion frame by applying motion vectors to the previous frame
    motion_frame = torch.clone(prev_frame)

    for b in range(batches):
        for f in range(n_frames):
            for i in range(len(motion[b, f])):
                mv = motion[b, f, i]
                mb_size = mv[0:2]

                # These are center points of the block
                src = mv[2:4]
                dst = mv[4:6]

                # convert from center point to block extents taking care of picture boundaries
                src_tl = torch.max(src - mb_size // 2, torch.zeros(2, device=device)).int()
                dst_tl = torch.max(dst - mb_size // 2, torch.zeros(2, device=device)).int()

                src_wh = torch.min(src_tl + mb_size, frame_size).int() - src_tl
                dst_wh = torch.min(dst_tl + mb_size, frame_size).int() - dst_tl

                real_wh = torch.min(src_wh, dst_wh)

                src_br = src_tl + real_wh
                dst_br = dst_tl + real_wh

                # copy the frame data
                motion_frame[b, f, :, dst_tl[1] : dst_br[1], dst_tl[0] : dst_br[0]] = prev_frame[b, f, :, src_tl[1] : src_br[1], src_tl[0] : src_br[0]]

    # The residual is then the difference between the decoded frame and the motion frame
    return motion_frame


def batch_mvs(mvs: Sequence[Tensor]) -> Tensor:
    max_vecs = max([m.shape[0] for m in mvs])
    mv_padding = [max_vecs - m.shape[0] for m in mvs]
    return torch.stack([pad(m, (0, 0, 0, p)) for m, p in zip(mvs, mv_padding)])


parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("--stream", type=int, default=0, required=False)
parser.add_argument("--cuda", action="store_true")
parser.add_argument("--save", action="store_true")
args = parser.parse_args()

if args.cuda:
    print("Using CUDA")
    device = torch.device("cuda")
else:
    print("Using CPU")
    device = torch.device("cpu")

container = av.open(args.input)

stream = container.streams.video[args.stream]
stream.codec_context.flags2 |= av.codec.context.Flags2.EXPORT_MVS

frame_iterator = container.decode(video=args.stream)

# extract the first frame, it should be an I frame
iframe_vid = next(frame_iterator)
assert iframe_vid.pict_type == PictureType.I

frame_volume = [to_tensor(iframe_vid.to_rgb().to_ndarray()).to(device)]
mvs = []

# Make a pixel volume out of the frames
for frame in frame_iterator:
    if frame.pict_type == PictureType.P:
        frame_volume.append(to_tensor(frame.to_rgb().to_ndarray()).to(device))
        mvs.append(extract_mvs(frame.side_data[0]).to(device))
    else:
        break

frame_volume = torch.stack(frame_volume[:-1]).repeat(5, 1, 1, 1, 1)  # simulate batch by copying data
mvs = batch_mvs(mvs).unsqueeze(0).repeat(5, 1, 1, 1)

print(f"Extracted frame volume of size {frame_volume.shape}")
print(f"Extracted motion vectors of size {mvs.shape}")
n_frames = frame_volume.shape[0] * frame_volume.shape[1]

vec_time = 0
loop_time = 0

if args.cuda:
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    start.record()
else:
    starttime = timeit.default_timer() * 1000

motion_frame = motion_compensation_vectorized(frame_volume, mvs, torch.as_tensor(frame_volume.shape, device=device))

if args.cuda:
    end.record()
    torch.cuda.synchronize()

    vec_time += start.elapsed_time(end)
else:
    endtime = timeit.default_timer() * 1000
    vec_time = endtime - starttime

if args.save:
    write_video("vectorized.mp4", (motion_frame * 255).byte().squeeze(0).permute(0, 2, 3, 1), fps=stream.average_rate)

print(f"Vector decoding time: {vec_time / n_frames}ms per frame ({n_frames / vec_time * 1000} fps)")

if args.cuda:
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    start.record()
else:
    starttime = timeit.default_timer() * 1000

motion_frame_loop = motion_compensation_loop(frame_volume, mvs)

if args.cuda:
    end.record()
    torch.cuda.synchronize()

    loop_time += start.elapsed_time(end)
else:
    endtime = timeit.default_timer() * 1000
    loop_time = endtime - starttime

if args.save:
    write_video("loop.mp4", (motion_frame_loop * 255).byte().squeeze(0).permute(0, 2, 3, 1), fps=stream.average_rate)

print(f"Loop decoding time: {loop_time / n_frames}ms per frame ({n_frames / loop_time * 1000} fps)")
