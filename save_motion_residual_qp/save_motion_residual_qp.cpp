/**
 * This example uses ffmpeg to parse the container format and to decode the resulting bitstream. 
 * I-Frames are fully decoded to RGB frames and saved as PPM images. P-frames are partially decoded by applying motion vectors to 
 * the previously decoded frame. The residual is then computed by subtracting the motion-vector applied result from
 * the fully decoded frame. The residual is saved as a PPM image and the motion vectors are saved to a csv file. For all 
 * frames the quantization tables for each macroblock are also saved.
 * This code assumes that the underlying codec:
 *  (a) Uses full-frames, i.e., it does not use slices
 *  (b) The FFMPEG implementation supports exporting motion vectors
 *  (c) The codec only references the previous frame (no multiple anchors, no B frames)
 */

// FFMPEG headers are pure C, they need to be surrounded by extern "C" to avoid name mangling
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libavutil/motion_vector.h>
#include <libavutil/video_enc_params.h>
#include <libswscale/swscale.h>
}

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <optional>
#include <sstream>
#include <vector>

void write_ppm(const std::string &fname, AVFrame *frame) {
    // PPM file format:
    // Line 1: format identifier P6
    // Line 2: width <space> height <space> max_value (255)
    // Line 3: raw RGB image data
    std::ofstream output_image;
    output_image.open(fname, std::ios::binary | std::ios::out);
    output_image << "P6\n"
                 << frame->width << " " << frame->height << "\n255\n";

    for (int y = 0; y < frame->height; y++) {
        output_image.write((const char *)frame->data[0] + (y * frame->linesize[0]), frame->width * 3);
    }
}

void write_qp(const std::string &fname, AVFrame *frame, const std::vector<int8_t> &qps) {
    // QP format
    // Line 1: width <space> height <space> macroblock_size (always 16 for now)
    // Line 2: quantization parameters space separated (single integer per macroblock)
    std::ofstream output;
    output.open(fname);
    output << frame->width << " " << frame->height << " 16\n";

    std::transform(qps.begin(), qps.end(), std::ostream_iterator<std::string>(output, " "), [](auto q) { return std::to_string(q); });
}

int main(int argc, char **argv) {
    // Read the container and set up the streams and codecs, see full_decode for more details
    AVFormatContext *format_context = nullptr;

    if (avformat_open_input(&format_context, argv[1], nullptr, nullptr) != 0) {
        std::cout << "Unable to open " << argv[1] << std::endl;
        return -1;
    }

    if (avformat_find_stream_info(format_context, nullptr) < 0) {
        std::cout << "Unable to parse video streams, video file is invalid" << std::endl;
    }

    av_dump_format(format_context, 0, argv[1], 0);

    AVCodec *codec = nullptr;
    int stream = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);

    if (stream < 0) {
        std::cout << "Unable to find a video stream in the file" << std::endl;
        return -1;
    }

    AVCodecParameters *codec_params = format_context->streams[stream]->codecpar;

    AVCodecContext *codec_context = avcodec_alloc_context3(codec);
    avcodec_parameters_to_context(codec_context, codec_params);

    AVDictionary *opts = nullptr;
    av_dict_set(&opts, "flags2", "+export_mvs", 0);
    av_dict_set(&opts, "export_side_data", "venc_params", 0);

    if (avcodec_open2(codec_context, codec, &opts) < 0) {
        std::cout << "Unable to load codec" << std::endl;
        return -1;
    }

    // Setup the frames
    AVFrame *nativeFrame = av_frame_alloc();
    AVFrame *rgbFrame = av_frame_alloc();

    av_image_alloc(rgbFrame->data, rgbFrame->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
    rgbFrame->width = codec_params->width;
    rgbFrame->height = codec_params->height;

    SwsContext *sws_context = sws_getContext(codec_params->width, codec_params->height, (AVPixelFormat)codec_params->format, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);

    // Parse the stream
    int framenum = 0;
    for (AVPacket packet; av_read_frame(format_context, &packet) >= 0; framenum++) {
        if (packet.stream_index == stream) {
            // Unfortunately the frame still needs to be fully decoded to get motion vectors
            avcodec_send_packet(codec_context, &packet);
            avcodec_receive_frame(codec_context, nativeFrame);

            if (nativeFrame->pict_type == AV_PICTURE_TYPE_I) {
                // I-Frames always replace the reference frame
                sws_scale(sws_context, nativeFrame->data, nativeFrame->linesize, 0, nativeFrame->height, rgbFrame->data, rgbFrame->linesize);

                std::ostringstream fname_format;
                fname_format << "frame" << framenum << ".ppm";

                write_ppm(fname_format.str(), rgbFrame);
            } else {
                // P-frame
                // Get the motion vectors as side data
                AVFrameSideData *sd = av_frame_get_side_data(nativeFrame, AV_FRAME_DATA_MOTION_VECTORS);
                AVMotionVector *mvs = reinterpret_cast<AVMotionVector *>(sd->data);
                int n = sd->size / sizeof(AVMotionVector);

                // Now things get complicated
                // We allocate space for the residual
                // FFMPEG doesn't allow access to coefficients or residuals, making it extra dumb (and slow and with memory overhead) for this kind of
                // bitstream analysis. But you can do it yourself by computing the image from motion vectors alone and subtracting
                // fully decoded frame. Because the transform is linear, this gives you (mostly) the same residual that would have been stored (but in RGB space)
                AVFrame *residual = av_frame_alloc();

                av_image_alloc(residual->data, residual->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
                residual->width = codec_params->width;
                residual->height = codec_params->height;

                // Allocate a frame for the decoded P-frame
                AVFrame *currentFrame = av_frame_alloc();

                av_image_alloc(currentFrame->data, currentFrame->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
                currentFrame->width = codec_params->width;
                currentFrame->height = codec_params->height;

                // Convert the decoded P-frame to RGB
                sws_scale(sws_context, nativeFrame->data, nativeFrame->linesize, 0, nativeFrame->height, currentFrame->data, currentFrame->linesize);

                // Make a file to store the motion vector
                std::ostringstream motion_fname_format;
                motion_fname_format << "frame" << framenum << "_motion.csv";

                std::ofstream output_motion;
                output_motion.open(motion_fname_format.str());
                output_motion << "source,width,height,src_x,src_y,dst_x,dst_y,motion_x,motion_y,motion_scale" << std::endl;

                // Start processing motion vectors
                for (auto it = mvs; it != mvs + n; it++) {
                    output_motion << it->source << "," << int(it->w) << "," << int(it->h) << "," << it->src_x << "," << it->src_y << "," << it->dst_x << "," << it->dst_y << "," << it->motion_x << "," << it->motion_y << "," << it->motion_y << std::endl;

                    for (int y = -it->h / 2; y < it->h / 2; y++) {
                        for (int x = -it->w / 2; x < it->w / 2; x++) {
                            // Compute the source pixel
                            int src_x = it->src_x + x;
                            int src_y = it->src_y + y;

                            if (src_x < 0 || src_x >= nativeFrame->width || src_y < 0 || src_y >= nativeFrame->height) { // Make sure it isnt out of bounds, this CAN happen
                                continue;
                            }

                            // Compute the destination pixel
                            int dst_x = it->dst_x + x;
                            int dst_y = it->dst_y + y;

                            if (dst_x < 0 || dst_x >= nativeFrame->width || dst_y < 0 || dst_y >= nativeFrame->height) { // Make sure it also isnt out of bounds
                                continue;
                            }

                            // Convert to location in the buffer
                            int src_offset = src_y * rgbFrame->linesize[0] + src_x * 3;
                            int dst_offset = dst_y * residual->linesize[0] + dst_x * 3;

                            // Compute the residual by subtracting the previous frame's source pixel from the current frames destination pixel and store it in the residual's destination pixel
                            // Note that I'm taking the absolute value to make the visualization cleaner
                            for (int c = 0; c < 3; c++) {
                                *(residual->data[0] + dst_offset + c) = std::abs(*(currentFrame->data[0] + dst_offset + c) - *(rgbFrame->data[0] + src_offset + c));
                            }
                        }
                    }
                }

                // save the residual to disk
                std::ostringstream residual_fname_format;
                residual_fname_format << "frame" << framenum << "_residual.ppm";

                write_ppm(residual_fname_format.str(), residual);

                // swap in the new reference frame
                std::swap(currentFrame, rgbFrame);
                av_frame_unref(currentFrame);
            }

            // For every frame, get the quantization parameters
            // This is HIGHLY codec dependent, and the generic function is deprecated
            // so this likely won't work generally and wont work in the future
            // Some codecs let you get the QPs from side-data, which this deprecated function encapsulates
            int stride, type;
            int8_t *qp_ptr = av_frame_get_qp_table(nativeFrame, &stride, &type);

            std::vector<int8_t> qps;
            if (qp_ptr) {
                // If that worked we can extract the QPs, there's one per macroblock
                // We have to assume 16x16 macroblocks since we don't have macroblock information
                unsigned int mb_h = (nativeFrame->height + 15) / 16; // the +15 is a simple trick to account for padding
                unsigned int mb_w = (nativeFrame->width + 15) / 16;
                unsigned int nb_mb = mb_h * mb_w;

                qps.resize(nb_mb);

                for (int y = 0; y < mb_h; y++) {
                    std::copy_n(qp_ptr + (y * stride), mb_w, qps.begin());
                }
            } else {
                // If that failed, then the codec may have this data in the encoding parameters
                // H264 in particular does this
                AVFrameSideData *sd = av_frame_get_side_data(nativeFrame, AV_FRAME_DATA_VIDEO_ENC_PARAMS);

                if (sd) {
                    // Extracting the qp information from this more complex
                    AVVideoEncParams *vep = (AVVideoEncParams *)sd->data;

                    if (vep->nb_blocks == 0) {
                        // Sometimes encoding params don't report blocks, if that's the case we can still save the per-frame QP
                        qps.push_back(vep->qp);
                    } else {
                        std::cout << vep->nb_blocks << " (" << sizeof(vep->nb_blocks) * 8 << "), "
                                  << vep->blocks_offset << " (" << sizeof(vep->blocks_offset) * 8 << "), "
                                  << vep->block_size << " (" << sizeof(vep->block_size) * 8 << "), "
                                  << vep->type << " (" << sizeof(vep->type) * 8 << "), "
                                  << vep->qp << " (" << sizeof(vep->qp) * 8 << "), " << std::endl;

                        for (int i = 0; i < 4; i++) {
                            std::cout << "DC: " << vep->delta_qp[i][0] << " AC: " << vep->delta_qp[i][1] << std::endl;
                        }
                        std::cout << "(" << sizeof(vep->delta_qp[0][0]) * 8 << ")" << std::endl;
                        return 0;

                        for (int block_idx = 0; block_idx < vep->nb_blocks; block_idx++) {
                            // We're still going to assume 16x16 macroblocks although using this structure you could get the exact sizes
                            // This is more relevant for codecs with variable macroblock sizes
                            AVVideoBlockParams *mb = av_video_enc_params_block(vep, block_idx);
                            qps[block_idx] = vep->qp + mb->delta_qp; // qp is defined with an initial QP plus an QP offset for each macroblock (and technically each channel too)
                        }
                    }
                }
            }

            if (qps.size() == 0) {
                std::cout << "Unable to read QP data, potentially incomaptible codec" << std::endl;
            } else {
                // and save them to disk
                std::ostringstream qp_fname_format;
                qp_fname_format << "frame" << framenum << "_quantization.txt";

                write_qp(qp_fname_format.str(), nativeFrame, qps);
            }
        }

        av_packet_unref(&packet);
    }

    av_frame_free(&nativeFrame);

    avcodec_close(codec_context);
    avcodec_free_context(&codec_context);
    avformat_close_input(&format_context);
    avformat_free_context(format_context);

    return 0;
}