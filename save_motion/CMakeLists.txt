add_executable(save_motion save_motion.cpp)
target_include_directories(save_motion PRIVATE ${AVCODEC_INCLUDE_DIR} ${AVFORMAT_INCLUDE_DIR} ${SWSCALE_INCLUDE_DIR} ${AVUTIL_INCLUDE_DIR}) 
target_link_libraries(save_motion PRIVATE ${AVCODEC_LIBRARY} ${AVFORMAT_LIBRARY} ${SWSCALE_LIBRARY} ${AVUTIL_LIBRARY}) 