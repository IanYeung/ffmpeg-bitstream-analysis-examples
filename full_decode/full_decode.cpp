/**
 * This example uses ffmpeg to parse the container format and compressed bitstream and to convert
 * the resulting images to RGB. The frames are written as PPM files. Supply the file name as the 
 * only argument to the program. This code makes no assumptions about the format of the video.
 * 
 * The file below is extensively commented to explain what is happening
 */

// FFMPEG headers are pure C, they need to be surrounded by extern "C" to avoid name mangling
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include <fstream>
#include <iostream>
#include <sstream>

void write_ppm(const std::string &fname, AVFrame *frame) {
    // PPM file format:
    // Line 1: format identifier P6
    // Line 2: width <space> height <space> max_value (255)
    // Line 3-end: raw RGB image data
    std::ofstream output_image;
    output_image.open(fname, std::ios::binary | std::ios::out);
    output_image << "P6\n"
                 << frame->width << " " << frame->height << "\n255\n";

    for (int y = 0; y < frame->height; y++) {
        output_image.write((const char *)frame->data[0] + (y * frame->linesize[0]), frame->width * 3);
    }
}

int main(int argc, char **argv) {
    // Read the container file, AVFormatContext encapsulates the container formats, it also managed to open file for you
    AVFormatContext *format_context = nullptr;

    if (avformat_open_input(&format_context, argv[1], nullptr, nullptr) != 0) {
        std::cout << "Unable to open " << argv[1] << std::endl;
        return -1;
    }

    // Parse the container and find streams
    if (avformat_find_stream_info(format_context, nullptr) < 0) {
        std::cout << "Unable to parse video streams, video file is invalid" << std::endl;
        return -1;
    }

    // Write a description of the container to stdout, this tells the user about the streams and their format
    av_dump_format(format_context, 0, argv[1], 0);

    // Next find the stream number of the "best" stream and get it's codec
    // "best" in this case means the one the user most likely wants to view
    AVCodec *codec = nullptr;
    int stream = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);

    if (stream < 0) {
        std::cout << "Unable to find a video stream in the file" << std::endl;
        return -1;
    }

    // Next we need to prepare a CodecContext which holds information about the stream as it's being parsed
    AVCodecParameters *codec_params = format_context->streams[stream]->codecpar;
    AVCodecContext *codec_context = avcodec_alloc_context3(codec);
    avcodec_parameters_to_context(codec_context, codec_params);

    // And with the codec and it's context, we can open the codec, preparing it for data
    if (avcodec_open2(codec_context, codec, nullptr) < 0) {
        std::cout << "Unable to load codec" << std::endl;
        return -1;
    }

    // Next we allocate two frame objects, one will hold the frame in the native format of the bitstream
    // The other will hold the frame after conversion to RGB
    AVFrame *nativeFrame = av_frame_alloc(); // Frame as decoded from the video (likely YCbCr)
    AVFrame *rgbFrame = av_frame_alloc();    // Frame after conversion to RGB24

    // The RGB frame needs its buffer allocated, the native frame will be allocated by the codec
    av_image_alloc(rgbFrame->data, rgbFrame->linesize, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, 32);
    rgbFrame->width = codec_params->width; // For some reason the width and height need to be set manually
    rgbFrame->height = codec_params->height;

    // Next we make the SwsContext which holds parameters related to scaling and converting the native frame to RGB
    SwsContext *sws_context = sws_getContext(codec_params->width, codec_params->height, (AVPixelFormat)codec_params->format, codec_params->width, codec_params->height, AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);

    // Finally we can start processing the video stream
    // We do this by reading packets from the container sequentially until we get to the stream we want, then
    // we can start processing them.
    int framenum = 0;
    for (AVPacket packet; av_read_frame(format_context, &packet) >= 0; framenum++) {
        if (packet.stream_index == stream) {
            // To decode the frame, we first send the packet to the codec to be parsed, then we read back the
            // parsed frame
            int ret = avcodec_send_packet(codec_context, &packet);
            ret = avcodec_receive_frame(codec_context, nativeFrame);

            // Next we convert the frame from the native format to RGB
            sws_scale(sws_context, nativeFrame->data, nativeFrame->linesize, 0, nativeFrame->height, rgbFrame->data, rgbFrame->linesize);

            // And write it to disk
            std::ostringstream fname_format;
            fname_format << "frame" << framenum << ".ppm";

            write_ppm(fname_format.str(), rgbFrame);
        }
        // Any packet we read needs to be freed
        av_packet_unref(&packet);
    }

    // Cleanup
    av_frame_free(&nativeFrame);

    av_freep(&rgbFrame->data); // we allocated this ourselves so it needs to be freed manually
    av_frame_free(&rgbFrame);

    avcodec_close(codec_context);
    avcodec_free_context(&codec_context);
    avformat_close_input(&format_context);
    avformat_free_context(format_context);

    return 0;
}